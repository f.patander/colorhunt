# ColorHunt

Paintball First Person Shooter

Player controls:
  - arrows: movement
  - mouse: aim
  - left mouse button (or l-ctrl): shoot
  - space: jump
  - z: crouch
  - k: suicide


Features:
  - [x] menu with settings
  - [x] network lobby
  - [x] player movements: shoot, jump, crouch
  - [x] paintball and paintstain (becomes child of hit object)
  - [x] teams
  - [x] HUD with minimap, gunsight and scoring
  - [x] scoreboard
  - [x] in-game menu (disconnect and volume control)
  - [x] player model (replace capsule), animations
  - [x] background music in menu and during game
  - [x] respawn

