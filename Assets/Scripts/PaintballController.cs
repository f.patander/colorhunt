﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class PaintballController: MonoBehaviour {

  public Material redPaintTexture;
  public Material bluePaintTexture;
  private Material texture;
  
  public int team;
  
  
  float lifespan = 5.0f;
  public GameObject paintstain;
  public PlayerController shooterPlayer;
  
  public static int maxPaintstain=100;
  public static List<GameObject> paintstainList = new List<GameObject>();
  
  
  
  // Start is called before the first frame update
  void Start() {
    Debug.Log("Paintball spawned on client team: "+team);
    //A seconda del team la pallina lascia una macchia di vernice di colore diverso
    if(team == 0)  {
      GetComponent<MeshRenderer>().material.color = Color.red;
      texture = redPaintTexture;
    } else {
      GetComponent<MeshRenderer>().material.color = Color.blue;
      texture = bluePaintTexture;
    }
    
  }
  

  // Update is called once per frame
  void Update()  {
  //Originariamente le macchie di vernice sparivano dopo tot tempo
//     lifespan -= Time.deltaTime;
//     if(lifespan<=0) {
//       Explode();
//     }
  }
  
  
  void OnCollisionEnter(Collision collision) {
    Debug.Log("C'è stata una collisione!");
    GameObject newPaintstain = (GameObject) Instantiate(paintstain);
    newPaintstain.transform.position = collision.contacts[0].point+collision.contacts[0].normal*(0.0001f);
    newPaintstain.transform.rotation = Quaternion.LookRotation(-collision.contacts[0].normal);
    
    newPaintstain.transform.SetParent(collision.contacts[0].otherCollider.GetComponentInParent<Transform>().transform);
    
    newPaintstain.GetComponent<MeshRenderer>().material = texture;
    //Decommentare per sapere che oggetto è stato colpito
//     Debug.Log("Hit: "+collision.gameObject);

//     Debug.Log("paintstainList.count = "+paintstainList.Count);
    //Se si raggiunte il numero massimo di macchie di vernice in gioco si cancellano quelle più vecchie
    if(paintstainList.Count == maxPaintstain) {
      GameObject last = paintstainList[0];
      paintstainList.Remove(last);
      Destroy(last);
    }
    paintstainList.Add(newPaintstain);

    
    
    if(shooterPlayer.isServer) {
      if(collision.gameObject.tag == "PlayerCollider") {
        Debug.Log("Colpito un giocatore");
        PlayerController hittedPlayerController=collision.gameObject.GetComponentInParent<PlayerController>();
        //Discriminare se è nemico
        if(this.team != hittedPlayerController.team) {
          Debug.Log("Colpito un nemico");
          NetworkManager_Custom nm = (NetworkManager_Custom) NetworkManager.singleton;
          nm.game.GetComponent<Game>().CmdScoreFor(this.team);
          
          //Si aggiornano i valori kills e deaths sul server, essendo sync var si aggiornano i rispettivi valori sui clients
          shooterPlayer.GetComponent<PlayerController>().kills+=1;
          hittedPlayerController.deaths+=1;
          
          nm.respawnPlayer(hittedPlayerController);
//           nm.game.GetComponent<Game>().CmdKillFor(shooterPlayer.GetComponent<PlayerController>());
//           nm.game.GetComponent<Game>().CmdDeathFor(hittedPlayerController);
        } else {
          Debug.Log("Colpito un alleato");
        }
        
      }
    }

    
    //Ultima cosa da effettuare (altrimenti si perde questo oggetto):
    //Rimozione della pallina di vernice dopo aver creato la paintstain
    Destroy(gameObject);
  } //OnCollisionEnter
  
  
  
  void Explode() {
      Destroy(gameObject);
  }
  
}
