﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetworkManager_Custom : NetworkManager {

  public string userName;
  SpawnSpot[] spawnSpots;
  public float respawnTimer = 5.0f;
  
  [SerializeField]
  GameObject game_prefab;
  
  public GameObject game;
  
  private int numberOfPlayer = 0;

  
	public void StartupHost() {
		SetPort();
		
		userName = GameObject.Find("NameInput").GetComponent<InputField>().text;
    
		NetworkManager.singleton.StartHost();
// 		Debug.Log(""+GameObject.Find("MapDropdown").GetComponent<Value>().value);
// 		NetworkManager.singleton.ServerChangeScene("FaceClassic");
	}
	
	
	public override void OnServerReady(NetworkConnection conn) {
    if(!game) {
      game = (GameObject)Instantiate(game_prefab);
      NetworkServer.Spawn(game);
    }
	} 

	public void JoinGame() {
		SetIPAddress();
// 		Debug.Log("JoinGame-SetIPAddress-"+NetworkManager.singleton.networkAddress);
		SetPort();
    
    userName = GameObject.Find("NameInput").GetComponent<InputField>().text;
	
// 		Debug.Log("JoinGame-SetPort-"+NetworkManager.singleton.networkPort);
		NetworkManager.singleton.StartClient();
		Debug.Log("Client Started!");
	}

  
	void SetIPAddress() {
		string ipAddress = GameObject.Find("IPText").GetComponent<Text>().text;
// 		string ipAddress = GameObject.Find("IPInput").transform.Find("Text").GetComponent<Text>().text;
		NetworkManager.singleton.networkAddress = ipAddress;
	}

	
	void SetPort() {
		NetworkManager.singleton.networkPort = 7777;
	}

	
	void OnLevelWasLoaded (int level) {
		if(level == 0) {
      SetupMenuSceneButtons();
      
      //FIXME
      StartCoroutine(SetupMenuSceneButtons());
      Debug.Log("OnLevelWasLoaded - level==0");
		}

		else
		{
      Debug.Log("OnLevelWasLoaded - Else");
      
      spawnSpots = GameObject.FindObjectsOfType<SpawnSpot>();
      Debug.Log("spawnSpots= "+spawnSpots.Length);
// 			SetupOtherSceneButtons();
    }
  }

  
	
	
	
	
	//serve per riabilitare i bottoni ricaricando la scene...
	IEnumerator SetupMenuSceneButtons()	{
    yield return new WaitForSeconds(0.3f);
		GameObject.Find("HostButton").GetComponent<Button>().onClick.RemoveAllListeners();
    GameObject.Find("HostButton").GetComponent<Button>().onClick.AddListener(StartupHost);

		GameObject.Find("JoinButton").GetComponent<Button>().onClick.RemoveAllListeners();
		GameObject.Find("JoinButton").GetComponent<Button>().onClick.AddListener(JoinGame);
	}

	
	//Forse non necessario
// 	void SetupOtherSceneButtons()
// 	{
// 		GameObject.Find("ButtonDisconnect").GetComponent<Button>().onClick.RemoveAllListeners();
// 		GameObject.Find("ButtonDisconnect").GetComponent<Button>().onClick.AddListener(NetworkManager.singleton.StopHost);
// 	}



  public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {
//     if (extraMessageReader != null) {
//       var s = extraMessageReader.ReadMessage<PlayerAddMessage>();
//       playerName = s.name;
//     }
//     PlayerController.playerNumber++;
//     Debug.Log("Number of player: "+PlayerController.playerNumber);
    
    
    SpawnSpot mySpawnSpot = spawnSpots[Random.Range(0, spawnSpots.Length)];
    if(spawnSpots==null) {
      Debug.LogError("Attention: no spawn spots available");
      return;
    }
    
    //Spawn in the origin
//     var player = (GameObject)GameObject.Instantiate(playerPrefab, new Vector3(0,0,0),Quaternion.identity);
    //Spawn in spawn spot position. Si può anche prendere l'orientamento se si desidera...
    var player = (GameObject)GameObject.Instantiate(playerPrefab, mySpawnSpot.transform.position,Quaternion.identity);
    
    //Team assinging
//     player.GetComponent<PlayerController>().team = PlayerController.playerNumber % 2;
    player.GetComponent<PlayerController>().team = numberOfPlayer % 2;
    numberOfPlayer+=1;
    
    NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
    
  //   player.GetComponent<PlayerController>().RpcInit(PlayerController.playerNumber % 2);
  //   player.GetComponent<Player>().Init();
  } 
  
  
  //Serve nell'update per respawnare... vedere di utilizzare anche in OnServerAddPlayer
//   void SpawnPlayer() {
//     SpawnSpot mySpawnSpot = spawnSpots[Random.Range(0, spawnSpots.Length)];
//     if(spawnSpots==null) {
//       Debug.LogError("Attention: no spawn spots available");
//       return;
//     }
//     
//     //Spawn in the origin
// //     var player = (GameObject)GameObject.Instantiate(playerPrefab, new Vector3(0,0,0),Quaternion.identity);
//     //Spawn in spawn spot position. Si può anche prendere l'orientamento se si desidera...
//     var player = (GameObject)GameObject.Instantiate(playerPrefab, mySpawnSpot.transform.position,Quaternion.identity);
//     
//     //Team assinging
//     player.GetComponent<PlayerController>().team = PlayerController.playerNumber % 2;
//     
//     player.GetComponent<PlayerController>().userName = userName;
//     
//     NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
//   }
  
  
  
  
  public void respawnPlayer(PlayerController hittedPlayerController) {
    Debug.Log("respawning...");
    
    //select a random spawn spot
    int n = 0;
    SpawnSpot spawn;
    do {
      ++n;
      spawn = spawnSpots[Random.Range(0, spawnSpots.Length)];
    } while (n < 5 && (spawn.transform.position-hittedPlayerController.transform.position).magnitude < 1);
    
    if(spawn == null) Debug.Log("No spawn spot selected");
    
    var newPlayerInstance = (GameObject)GameObject.Instantiate(playerPrefab,spawn.transform.position,Quaternion.identity);
    var newPlayer = newPlayerInstance.GetComponent<PlayerController>();
    newPlayer.team = hittedPlayerController.team;
    
    if (hittedPlayerController.miniCamObject != null) {
      hittedPlayerController.miniCamObject.transform.SetParent(newPlayer.transform);
      newPlayer.miniCamObject = hittedPlayerController.miniCamObject;
      hittedPlayerController.miniCamObject = null;
    }
    
    NetworkServer.Destroy(hittedPlayerController.gameObject);
    NetworkServer.ReplacePlayerForConnection(hittedPlayerController.connectionToClient, newPlayerInstance, hittedPlayerController.playerControllerId);
    Debug.Log("Respawned");
  }
  
  

  
  void Update() {
    if(respawnTimer>0) {
      respawnTimer -= Time.deltaTime;
      
      if(respawnTimer <= 0) {
        //Time to respawn the player!
      }
    }
    
    
  }





} //NetworkManager_Custom
