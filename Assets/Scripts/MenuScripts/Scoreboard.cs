﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoreboard : MonoBehaviour {

  [SerializeField]
  GameObject playerScoreboardItem;
  
  [SerializeField]
  Transform playerScoreboardList;

  void OnEnable() {
    //Get an array of players
    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
    
//     foreach(GameObject p in players) {
//       Debug.Log(p.GetComponent<PlayerController>().name + " | "+
//                 p.GetComponent<PlayerController>().kills+ " | "+
//                 p.GetComponent<PlayerController>().deaths);
//     }
    

    //Loop through and set up a list item for each OnEnable
      //Setting the UI elements equal to the relevant data
    foreach(GameObject p in players) {
      GameObject itemGO = Instantiate(playerScoreboardItem, playerScoreboardList);
      PlayerScoreboardItem item = itemGO.GetComponent<PlayerScoreboardItem>();
      if(item != null) {
        item.Setup(p.GetComponent<PlayerController>().playerName, p.GetComponent<PlayerController>().kills, p.GetComponent<PlayerController>().deaths, p.GetComponent<PlayerController>().team);
      }
    }
      
  }
  

  void OnDisable() {
    //Clean up our list of items
    foreach(Transform child in playerScoreboardList) {
      Destroy(child.gameObject);
    }
  }

  
}

