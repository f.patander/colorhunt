﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerScoreboardItem : MonoBehaviour {

  [SerializeField]
  Text usernameText;
  
  [SerializeField]
  Text killsText;
  
  [SerializeField]
  Text deathsText;
  
  public void Setup(string username, int kills, int deaths, int team) {
    if(team == 0) {
      usernameText.color = Color.red;
      killsText.color = Color.red;
      deathsText.color = Color.red;
    } else {
      usernameText.color = Color.blue;
      killsText.color = Color.blue;
      deathsText.color = Color.blue;
    }
    
    usernameText.text = username;
    killsText.text = ""+kills;
    deathsText.text = ""+deaths;
  }

}
