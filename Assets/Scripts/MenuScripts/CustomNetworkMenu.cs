﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


// https://www.youtube.com/watch?v=Tyxo06M2j_M

public class CustomNetworkMenu : MonoBehaviour {
  
//   public GameObject[] playerPrefabs;
  
//   public void SelectPlayer();
  public string[] maps = new string[] {"SampleScene", "FaceClassic"};
  
  public void SelectMap(int i) {
    //TODO: select map
//     NetworkManager.singleton.ServerChangeScene(sceneName);
    Debug.Log("Selected map: "+i);
    NetworkManager.singleton.onlineScene=maps[i];
    
  }
  
  
  public void SetupMenu() {
    //Si imposta come onlineScene quella selezionata di default nel dropdown menù
    NetworkManager.singleton.onlineScene=maps[GameObject.Find("MapDropdown").GetComponent<Dropdown>().value];
//     GameObject.Find("HostButton").GetComponent<Button>().onClick.RemoveAllListeners();
//     GameObject.Find("HostButton").GetComponent<Button>().onClick.AddListener(StartServer);
  
    GameObject.Find("MapDropdown").GetComponent<Dropdown>().onValueChanged.RemoveAllListeners();
    GameObject.Find("MapDropdown").GetComponent<Dropdown>().onValueChanged.AddListener(SelectMap);
    
//     GameObject.Find("JoinButton").GetComponent<Button>().onClick.RemoveAllListeners();
//     GameObject.Find("JoinButton").GetComponent<Button>().onClick.AddListener(JoinClient);

    
    GameObject.Find("ExitButton").GetComponent<Button>().onClick.RemoveAllListeners();
    GameObject.Find("ExitButton").GetComponent<Button>().onClick.AddListener(DoQuit);


  }
  
  

  public void StartServer() {
    Debug.Log("Setted up host onClick Listener");
    //Commentato in quanto assegnata come funzione onClick StartupHost in NetworkManager_Custom.cs - Decommentando anche qui si ha un errore perché cerca di aprire due connessioni
//     NetworkManager.singleton.StartHost();
  }
  
  
  
//   public void JoinClient() {
//     string ip = GameObject.Find("IPText").GetComponent<Text>().text;
//     string port = GameObject.Find("PortText").GetComponent<Text>().text;
//     if(ip.Length > 0)
//       NetworkManager.singleton.networkAddress = ip;
//     
//     
//     if(port.Length > 0) { 
//       int x;
//       int.TryParse(port, out x);
//       NetworkManager.singleton.networkPort = x;
//     }
//     
//     NetworkManager.singleton.StartClient();
//   }
  
  
  
  
  
  private void OnEnable() {
    SceneManager.sceneLoaded += OnSceneLoaded;
  }
  
  void OnSceneLoaded(Scene scene, LoadSceneMode mode) {    
    if(scene.name == "NetworkLobbyScene") {
      //Se si è nella network lobby si abilitano i relativi bottoni
      SetupMenu();
    } else {
      //altrimenti è necessario abilitare quelli per scollegarsi
      Debug.Log("TODO: implement setup disconnect menu");
      
      
      //NetworkIdentity è associata a player che però non si ha il riferimento in questo punto
//       if (NetworkIdentity.isServer) {
//         Debug.Log("I'm the server (or host)");
//         NetworkManager.singleton.StopHost();
//       } else {
//         Debug.Log("I'm the client");
//         NetworkManager.singleton.StopClient();
//       }
//       SceneManager.LoadScene("NetworkLobbyScene");
    
    
    }
  }
  
  

  
  public void Disconnect() {
    GameObject[] Players = GameObject.FindGameObjectsWithTag ("Player");
    foreach (GameObject player in Players) {
      if(player.GetComponent<NetworkIdentity>().isLocalPlayer) {
        Debug.Log("Disconnecting local player...");
        if(player.GetComponent<NetworkIdentity>().isServer) {
          NetworkManager.singleton.StopHost();
          Debug.Log("Stop host");
        } else {
          NetworkManager.singleton.StopClient();
          Debug.Log("Stop client");
        }
//         PlayerController.playerNumber-=1;
        SceneManager.LoadScene("NetworkLobbyScene");
      } //isLocal
    } //foreach
  }
  
  

  
  
  
  public void DoQuit() {
    Debug.Log("Exit button pressed");
    Application.Quit();
  }
  
  
  
  
  
}
