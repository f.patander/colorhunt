﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bg_gameMusic : MonoBehaviour {
  
  public List<AudioClip> bg_music = new List<AudioClip>();
  
  public int myIterator;
  
  AudioSource audioSource;
  bool paused = false;
  
  
  void Start() {
    
    System.Random rnd = new System.Random();
    myIterator = rnd.Next(bg_music.Capacity);
//     myIterator = Random.Next(bg_music.Capacity);
    
    audioSource = GetComponent<AudioSource>();
    
    audioSource.clip = bg_music[myIterator];
    audioSource.Play();
    paused = false;
    
//     GetComponent<AudioSource>().clip = bg_music[myIterator];
//     GetComponent<AudioSource>().Play();
  }
  
  void Update() {
    if (!paused && !audioSource.isPlaying) {
      if(myIterator >= bg_music.Capacity-1) {
        myIterator = 0;
      } else {
        myIterator++;
      }

      audioSource.clip = bg_music[myIterator];
      audioSource.Play();
    }
  }

  
  public void PauseSong() {
    if (paused) { audioSource.UnPause(); }
    else { audioSource.Pause(); }
    paused = !paused;
  }
  
  
  
  public void NextSong() {
    if(myIterator >= bg_music.Capacity-1) {
      myIterator = 0;
    } else {
      myIterator++;
    }
    
//     Debug.Log("myIterator: "+myIterator);
    audioSource.Stop();
    audioSource.clip = bg_music[myIterator];
    audioSource.Play();
    paused = false;
  }
  
  
  
  public void PrevSong() {
  Debug.Log("PREV "+myIterator);
    if(myIterator <= 0) {
      myIterator = bg_music.Capacity-1;
    } else {
      myIterator--;
    }
//     Debug.Log("myIterator: "+myIterator);

    GetComponent<AudioSource>().Stop();
    GetComponent<AudioSource>().clip = bg_music[myIterator];
    GetComponent<AudioSource>().Play();
    paused = false;
  }
  

  
  
  
  
}
