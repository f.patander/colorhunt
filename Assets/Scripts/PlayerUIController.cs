﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;


public class PlayerUIController : MonoBehaviour {

  [SerializeField]
  GameObject pauseMenu;
  
  [SerializeField]
  GameObject scoreboard;
  
  public bool gameIsPaused = false;

  // Start is called before the first frame update
  void Start() {
    if(scoreboard==null) {Debug.Log("Attenzione - no scoreboard object");}
    if(pauseMenu==null) {Debug.Log("Attenzione - no scoreboard object");}
    
    Bg_gameMusic gameMusic = null;
    foreach (var c in GetComponentsInChildren<Bg_gameMusic>()) {
      if (c.gameObject.name == "HUD") {
        gameMusic = c;
      }
    }
    
    foreach (var btn in pauseMenu.GetComponentsInChildren<Button>()) {
      if (btn.name == "PauseSongButton") {
        btn.onClick.AddListener(gameMusic.PauseSong);
      } else if (btn.name == "PrevSongButton") {
        btn.onClick.AddListener(gameMusic.PrevSong);
      } else if (btn.name == "NextSongButton") {
        btn.onClick.AddListener(gameMusic.NextSong);
      } else if (btn.name == "DisconnectButton") {
        btn.onClick.AddListener(GameObject.Find("NetworkManager_Custom").GetComponent<CustomNetworkMenu>().Disconnect);
      }
    }
    
    
  }

  // Update is called once per frame
  void Update() {
    //Update HUD - teams score
//     Debug.Log("TEXT: "+GameObject.Find("HUD").GetComponentsInChildren<Text>()[0].text + " "+GameObject.Find("HUD").GetComponentsInChildren<Text>()[1].text);
//     Debug.Log("VAR: red = "+Game.Instance.redScore + " blue="+Game.Instance.blueScore);
//     GameObject.Find("HUD").GetComponentsInChildren<Text>()[0].text = "Red Score: "+Game.Instance.redScore;
//     GameObject.Find("HUD").GetComponentsInChildren<Text>()[1].text = "Blue Score: "+Game.Instance.blueScore;
//     if() {
//       Game.Instance.RpcUpdateTextScore();
//     }
//     Debug.Log("RS: "+Game.Instance.redScore+" BS: "+Game.Instance.blueScore);
  
  
  
  
    //Attivazione/disattivazione in-game menu
    if(Input.GetKeyDown(KeyCode.Escape)) {
//       Debug.Log("Pressed Esc - gamePaused: "+gameIsPaused);
      if(gameIsPaused) {
        Resume();
      } else {
        Pause();
      }
      
    }
  
  
    //Attivazione/disattivazione scoreboard
    if(Input.GetKeyDown(KeyCode.Tab)) {
      scoreboard.SetActive(true);
    } else if(Input.GetKeyUp(KeyCode.Tab)) {
      scoreboard.SetActive(false);
    }
  }
  

  
  
  public void Resume() {
    pauseMenu.SetActive(false);
    Cursor.visible = false;
    Cursor.lockState = CursorLockMode.Locked;
    
    foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player")) {
      if(obj.GetComponent<NetworkIdentity>().isLocalPlayer) {
        obj.GetComponent<PlayerController>().enabled = true;
        obj.GetComponent<PlayerShooter>().enabled = true;
      }
    }
    
//     Time.timeScale=1.0f;
    gameIsPaused = false;
    
  }
  
  
  public void Pause() {
    pauseMenu.SetActive(true);
    Cursor.visible = true;
    Cursor.lockState = CursorLockMode.None;
      
    
    foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player")) {
      if(obj.GetComponent<NetworkIdentity>().isLocalPlayer) {
        obj.GetComponent<PlayerController>().enabled = false;
        obj.GetComponent<PlayerShooter>().enabled = false;
      }
    }
//     Time.timeScale=0.0f;
    gameIsPaused=true;
  }
  
  
  
  
  
  
  
}





