﻿/**
* Francesco Patander
* a.a. 2019-2020
* Sistemi di realtà virtuale
**/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI; 

[RequireComponent (typeof(CharacterController))]
// public class PlayerController : MonoBehaviour {
public class PlayerController : NetworkBehaviour {
  
  
  [SyncVar]
  public int team;
  [SyncVar(hook="OnChangeName")]
  public string playerName;
  
  [SyncVar]
  public int kills=0;
  [SyncVar]
  public int deaths=0;
  
  
  public float movementSpeed = 5.0f;
  public float mouseSensitivity = 5.0f;
  public float jumpSpeed = 5.0f;
  bool isCrouching = false;
  
  float verticalRotation = 0;
  public float upDownRange = 60.0f; //serve per impedire di ruotare completamente la camera

  float verticalVelocity = 0.0f;
  
  CharacterController characterController;
  
  public GameObject camObject;
//   public Camera cam; // Drag camera into here
  public GameObject miniCamObject;
  
 
  Animator anim;
 
  void OnChangeName(string newName) {
    GetComponentsInChildren<TextMesh>()[0].text = newName;
  }
  
  // Start is called before the first frame update
  void Start() {    
    if(!isLocalPlayer) {
      camObject.SetActive(false);
    }
  
    if(isLocalPlayer) {
      NetworkManager_Custom nm = (NetworkManager_Custom)NetworkManager_Custom.singleton;
      playerName = nm.userName;
      CmdChangePlayerName(playerName);
      Debug.Log("init name: "+nm.userName);
    }
    Debug.Log("name: "+playerName);
    GetComponentsInChildren<TextMesh>()[0].text = playerName;

  
  
    kills=0;
    deaths=0;
  
    if(team==0) {
      GetComponentsInChildren<MeshRenderer>()[0].material.color = Color.red;        //set name color
      GetComponentsInChildren<SkinnedMeshRenderer>()[0].material.color = Color.red; //set character color
    } else {
      GetComponentsInChildren<MeshRenderer>()[0].material.color = Color.blue;
      GetComponentsInChildren<SkinnedMeshRenderer>()[0].material.color = Color.blue;
    }
    
    anim = GetComponent<Animator>();

    Debug.Log("Player Initialized");

    //Se non è il giocatore locale ritorna
    if(!isLocalPlayer)
      return;
    
    
    Camera cam = camObject.GetComponent<Camera>();
    
    cam.enabled = true;
    miniCamObject = GameObject.Find("MiniCam");
    //resetposition
    GameObject.Find("MiniCam").transform.position = new Vector3(0, 15, 0);
    GameObject.Find("MiniCam").transform.position += this.transform.position;
    GameObject.Find("MiniCam").transform.SetParent(this.transform);
  
  
  //     Screen.lockCursor = true;
    Cursor.lockState = CursorLockMode.Locked;
    Cursor.visible = false;
    
    characterController = GetComponent<CharacterController>();
    //Si poteva aggiungere il controllo internamente ma abbiamo provato con l'inserimento della richiesta come specifica della classe
//     if(characterController==null) {}

  }
  
//   public override void OnStartClient() {
//     Debug.Log("HEY "+GameObject.Find("NameInput").GetComponent<InputField>().text);
//   }
  
  
  // Update is called once per frame
  void Update() {
    //XXX: aggiornamento mesh collider in base alla mesh corrente. Sconsigliato in quanto molto oneroso in potenza computazionale. La prassi più comune è utilizzare collider più semplici opportunamente dimensionati per ogni parte dell'oggetto e attaccarli al bone. In questo modo i vari collider seguiranno l'animazione.
    //Aggiornamento mesh prima del controllo se localPlayer altrimenti solo il giocatore locale avrà la mesh corretta, tutti gli altri manterranno la T pose
//     Debug.Log("Updating Mesh");
    Mesh colliderMesh = new Mesh();
    GetComponentsInChildren<SkinnedMeshRenderer>()[0].BakeMesh(colliderMesh);
    GetComponentsInChildren<MeshCollider>()[0].sharedMesh = null;
    GetComponentsInChildren<MeshCollider>()[0].sharedMesh = colliderMesh;
    
    
    //Non dovrebbe essere necessario perché, come mostrato in un tutorial, viene verificata la condizione nella funzione Start() ma la docs unity mette questo controllo di condizione.
    if (!isLocalPlayer)
      return;
      

    //Suicide button
    if(Input.GetKey("k")) {
      Debug.Log("Suicide");
      NetworkManager_Custom nm = (NetworkManager_Custom) NetworkManager.singleton;
      nm.respawnPlayer(this);
    }
      
      
      
    
    //Rotation
    float rotLeftRight = Input.GetAxis("Mouse X") * mouseSensitivity;
    transform.Rotate(0, rotLeftRight, 0); //YAW - spostandoci lateralmente con il mouse si compie una rotazione sull'asse verticale Y
    
//     float rotUpDown -= Input.GetAxis("Mouse Y") * mouseSensitivity; //PITCH - spostandoci verticalmente con il mouse si alza e si abbassa la vista
//     float currentUpDown = Camera.main.transform.rotation.eulerAngles.x;
//     float desiredUpDown = currentUpDown - rotUpDown;
//     desiredUpDown = Mathf.Clamp(desiredUpDown, -upDownRange, upDownRange);
//     Camera.main.transform.localRotation = Quaternion.Euler(desiredUpDown, 0, 0);
    verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
    verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);
    camObject.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);
    
    
    //Crouching
    if(Input.GetKey("z") && (isCrouching || characterController.isGrounded)) {
      characterController.height = 1.0f;
      isCrouching = true;
      anim.SetBool("Crouching", true);
      //transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
    } else {
      characterController.height = 2.0f;
      isCrouching = false;
      anim.SetBool("Crouching", false);
      //transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
    }
    
    
    
    //Movement
    float forwardSpeed = Input.GetAxis("Vertical") * movementSpeed; //assumerà valore negativo per andare indietro
    float sideSpeed = Input.GetAxis("Horizontal") * movementSpeed;
    //per implementare lo sprint (corsa/camminata) basta controllare se il tasto desiderato è premuto e nel caso moltiplicare la velocità per un fattore, esempio: 1.5
    
    verticalVelocity += Physics.gravity.y * Time.deltaTime;
    
    
    
    //Jumping
//     Debug.Log(characterController.isGrounded +" "+ !isCrouching +" "+ Input.GetButton("Jump"));
    if(characterController.isGrounded) {
      anim.SetBool("Jumping", false);
      if (!isCrouching && Input.GetButton("Jump")) {
        verticalVelocity = jumpSpeed;
        anim.SetBool("Jumping", true);
      } else {
        verticalVelocity=0.0f;
      }
    }
    //Old version without verticalVelocity reset
//     if(characterController.isGrounded && !isCrouching && Input.GetButton("Jump")) {
//       verticalVelocity = jumpSpeed;
//     } 
    
    
    Vector3 speed = new Vector3(sideSpeed, verticalVelocity, forwardSpeed);
    speed = transform.rotation*speed;
    
    
//     Vector3 animSpeed = speed;
// //     Debug.Log("side: "+sideSpeed+", vertical: "+verticalVelocity+", forward: "+forwardSpeed);
//     if(animSpeed.magnitude > 1.0f) {
//       animSpeed = speed.normalized;
//     }
//     
    
//     Debug.Log("AnimSpeed: "+animSpeed.magnitude);
//     anim.SetFloat("Speed", animSpeed.magnitude);
    
    Vector3 horSpeed = new Vector3(sideSpeed, 0, forwardSpeed);
    horSpeed = transform.rotation*horSpeed;
    
    anim.SetFloat("Speed", horSpeed.magnitude*(forwardSpeed>=0 ? 1 : -1));
//     if(verticalVelocity==0.0f) {
//     }
    
    
    characterController.Move(speed * Time.deltaTime);
    
  } //Update
  
  
  //Il proprio avatar viene colorato di rosso, funzione trovata nella docs, probabilmente non è quella giusta per questo caso
//   public override void OnStartLocalPlayer() {
//     GetComponent<MeshRenderer>().material.color = Color.red;
//   }

  

  [Command]
  void CmdChangePlayerName(string n) {
    Debug.Log("CmdChangePlayerName: "+n);
    playerName = n;
  }



  



} //PlayerController
