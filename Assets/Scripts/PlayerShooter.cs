﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerShooter : NetworkBehaviour {

  public GameObject bullet_prefab;
  float bulletImpulse = 50.0f;
  double lastShotTime;
  private const double minTimeBetweenShots = 0.3;
  
  
//   public ammoAmount = 50;

  // Start is called before the first frame update
  void Start()   {
    //Se non è il giocatore locale ritorna
    if(isLocalPlayer == false)
      return;
  
    lastShotTime = Time.time;
    
  }

  // Update is called once per frame
  void Update()   {
    
    if(isLocalPlayer == false)
      return;
    
    double currentTime = Time.time;
    if(Input.GetButton("Fire1") && ( (lastShotTime + minTimeBetweenShots) < currentTime) ) {
//       Debug.Log("lst= "+lastShotTime+ " ct = "+currentTime+"   mtbs "+minTimeBetweenShots);
      Camera cam = Camera.main;
      CmdShoot(cam.transform.forward);
      
      lastShotTime = currentTime;
    }
  }
  
  
  
  
  [Command]
  private void CmdShoot(Vector3 forward_c){
    Transform camTransform_s =gameObject.transform.Find("Main Camera");
    RpcMakeBullet(camTransform_s.position + forward_c,forward_c*bulletImpulse);
  }
  
  [ClientRpc]
  private void RpcMakeBullet(Vector3 pt, Vector3 initialImpulse) {
//     Camera cam = ;
    GameObject thebullet = (GameObject)Instantiate(bullet_prefab, pt, Quaternion.identity);
//     thebullet.GetComponent<Rigidbody>().AddForce( forward_c*bulletImpulse, ForceMode.Impulse );    
    thebullet.GetComponent<PaintballController>().team = GetComponent<PlayerController>().team;
    thebullet.GetComponent<Rigidbody>().AddForce( initialImpulse, ForceMode.Impulse );
    thebullet.GetComponent<PaintballController>().shooterPlayer = GetComponent<PlayerController>();
//     thebullet.GetComponent<PaintballController>().shooterPlayer = GetComponent<PlayerController>();

//     Debug.Log("Spawning bullet team: "+thebullet.GetComponent<PaintballController>().team +" CT: "+Time.time);
  }
  
  
}
