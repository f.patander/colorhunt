﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Game : NetworkBehaviour {

  private int redScore=0;
  private int blueScore=0;
  
  private int numberOfPlayer=-1;
  
  // Start is called before the first frame update
  public void Clear() {
    redScore=0;
    blueScore=0;
    
    numberOfPlayer=-1;
  }

//   // Update is called once per frame
//   void Update() {
//     
//   }
  
  
  
  [Command]
  public void CmdScoreFor(int team) {
    if(team==0) { redScore+=1; }
    else { blueScore+=1; }
    
    RpcUpdateScore(redScore, blueScore);
  }
  
  [ClientRpc]
  public void RpcUpdateScore(int rs, int bs) {
    GameObject.Find("RedScoreText").GetComponent<Text>().text = "Red Score: "+rs;
    GameObject.Find("BlueScoreText").GetComponent<Text>().text = "Blue Score: "+bs;
  }
  
  
  
}
